class slider {
    slideIndex = 1;
    constructor(options){
        this.options = options;

        this.initialstuff();

        this.createNextAndPrevbutt();
        this.createDots();
        this.showSlides(1);
        this.setInterval();

    }


    /**
     * for validation
     * return void
     */    
    initialstuff(){
        let {el:sliderElement , slideClass , auto} = this.options;
        if( !sliderElement ) throw Error('slider element is not exist');
        Number.isInteger(auto) ? this.auto = auto : this.auto = 0 ;

        this.sliders = [...sliderElement.children].filter(elm => elm.classList.contains(slideClass));
        
    }

    /**
     * for create next and prev buttons in slides
     * return void
     */ 
    createNextAndPrevbutt() {
        let {el:sliderElement} = this.options;
        sliderElement.insertAdjacentHTML('beforeend' ,`
            <a class = ' next '><<a>
            <a class = ' prev '>><a>
        `);
        sliderElement.querySelector('.next').addEventListener('click' ,() => this.nextAndPrevSlide( this.slideIndex += 1));
        sliderElement.querySelector('.prev').addEventListener('click' ,() => this.nextAndPrevSlide (this.slideIndex -= 1));
        
    }

    /**
     * for dinamic prev and next buttons
     * @param number 
     */
    nextAndPrevSlide = (n) => {
        this.resetInterval();
        this.showSlides(n);
    }
    /**
     * for dinamic dots
     *@param number 
     */
    currentSlide = (n) => {
        this.resetInterval();
        this.showSlides(this.slideIndex=n);
    }
    /**
     * for create dot element in ui
     */
    createDots() {
         let { el : sliderElement} = this.options;

        let dotElements = [...this.sliders].map((slider , index) => `<span class="dot" data-slide="${index+1}"></span>`)

        let dots = document.createElement('div')
        dots.classList.add('dots');
        dots.innerHTML = `${dotElements.join('')}`

        sliderElement.after(dots);

        this.dots = dots.querySelectorAll('.dot');
        this.dots.forEach(dot => dot.addEventListener('click', e => this.currentSlide(parseInt(e.target.dataset.slide))))
    }


    /**
     * for show slides
     * @param  number 
     */
    showSlides(number) {
        let { el : sliderElement , slideClass , currentslider } = this.options;
        
        if(number > this.sliders.length) this.slideIndex = 1;
        if(number < 1 ) this.slideIndex = this.sliders.length

        sliderElement.querySelector(`.${slideClass}.active`).classList.remove('active');
        this.dots.forEach(dot => dot.classList.remove('active'))

        this.sliders[this.slideIndex-1].classList.add('active');
        this.dots[this.slideIndex-1].classList.add('active')

        if(currentslider) currentslider(this.sliders[this.slideIndex-1]);
    }
    setInterval(){
        if(this.auto !== 0 ){
            this.intervalID = setInterval(() => this.showSlides(this.slideIndex += 1),this.auto);
        }
    }
    resetInterval(){
        clearInterval(this.intervalID);
        this.setInterval();
    }

}

